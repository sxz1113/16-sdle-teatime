# -*- coding: utf-8 -*-

# This file demonstrates some of the capabilities of NumPy for linear
# algebra

import numpy as np

# To make use of some of the methods for linear algebra we need the 
# submodule linalg
import numpy.linalg as npla

print ("import numpy as np\n")
print ("from numpy import linalg as npla\n")


# Define two matrices x and y
x = np.array([[1. , 0],[0, 1.]])
print("x = ")
print(x)
y = np.array([[2. , 1],[4, 5]])
print("y = ")
print(y)

# Element-wise multiplication
print("\nElement-wise multiplication using *: x*y")
print(x * y) # results into [[ 2.  0.], [ 0.  5.]]

# Matrix multiplication
print("\nMatrix multiplication using the dot method: np.dot(x,y)")
print(np.dot(x,y))

print("\nThis is equivalent to x.dot(y)")
print(x.dot(y))

# To transpose, use T
print("\nUsing .T we transpose a matrix: y.T")
print(y.T)

# To calculate the inverse of y we use inv
print("\nCalculate the inverse of y using the inv method in linalg: npla.inv(y)")
print(npla.inv(y))

# To get the determinant, use det
print("\nCalculate the determinant of y using det: npla.det(y)")
print(npla.det(y))

# Using the function ones!
print("\nWe use ones to generate a matrix with 1 in all the components: np.ones((3,3))")
print(np.ones((3,3)))

# USing the zeros function
print("\nWe use zeros to generate a matrix with 0 in all the components: np.zeros((3,3))")
print(np.zeros((3,3)))

